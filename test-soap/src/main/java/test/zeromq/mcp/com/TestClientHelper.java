/*
 * Copyright (c) 2020 by Mission Critical Partners and the Iowa Division of Criminal and Juvenile Justice Planning.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 * - Prior written consent by Mission Critical Partners is required.
 * - Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package test.zeromq.mcp.com;

import com.mcp.util.xml.BaseXmlBindingHelper;
import com.mcp.util.xml.BindingException;
import com.mcp.util.xml.JAXBUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by jmierwa on 7/17/2020.
 */
public class TestClientHelper extends BaseXmlBindingHelper {

    private static final Logger LOG = LoggerFactory.getLogger(TestClientHelper.class.getName());
    private static final ObjectFactory FACTORY = new ObjectFactory();
    private static final String SCHEMA_LOCATION = "wsdl/TestMsg.xsd";
    private static final String NAMESPACE = "urn:com.mcp.zeromq.test";
    private static final String ELEMENT_NAME = "test.zeromq.mcp.com";
    private static final String JAXB_CONTEXT_CLASSLIST = "test.zeromq.mcp.com";

    private static final String BINDING_ERROR_MSG = "Failed to strip false NILs from Marshalled data.";
    private TestMsgType jaxbObj;

    public TestClientHelper() {
        super(JAXB_CONTEXT_CLASSLIST, SCHEMA_LOCATION, NAMESPACE, ELEMENT_NAME);
    }

    public TestClientHelper(TestMsgType jaxbObj) {
        super(JAXB_CONTEXT_CLASSLIST, SCHEMA_LOCATION, NAMESPACE, ELEMENT_NAME);
        this.jaxbObj = jaxbObj;
    }

    @Override
    public JAXBElement<?> toJAXBElement(Serializable obj) {
        return FACTORY.createTestMsg((TestMsgType)obj);
    }

    /**
     * Returns an unmarshalled object from an input string using the initialized context.
     * @param is is The {@link java.io.InputStream} to be unmarshalled.
     * @return The unmarshalled object
     * @throws JAXBException when marshalling fails.
     * @throws BindingException {@link com.mcp.util.xml.BindingException} when marshalling or false nil stripping fails.
     */
    public static TestMsgType toObject(final String messageString) throws JAXBException, BindingException {
        return JAXBUtil.toObject(JAXB_CONTEXT_CLASSLIST, new TestMsgType(), messageString);
    }
}