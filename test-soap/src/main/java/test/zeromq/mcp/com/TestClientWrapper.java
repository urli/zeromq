
package test.zeromq.mcp.com;

import java.io.InputStream;
import java.util.Properties;
import javax.xml.namespace.QName;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import soap.test.zeromq.mcp.com.TestServicePort;

public final class TestClientWrapper {

    private static final QName SERVICE_NAME = new QName("urn:com.mcp.zeromq.test.soap", "TestService");

    private TestClientWrapper() {
    }

    public static String submit(String payloadString) throws java.lang.Exception {
        
        TestMsgType msgBindingObject = TestClientHelper.toObject(payloadString);
//        InputStream input = TestClientWrapper.class.getClassLoader().getResourceAsStream("/.application.properties");
        InputStream input = TestClientWrapper.class.getClassLoader().getResourceAsStream("application.properties");
        Properties prop = new Properties();
        prop.load(input);
        
        JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
        jaxWsProxyFactoryBean.setServiceClass(TestServicePort.class);
        jaxWsProxyFactoryBean.setAddress(prop.getProperty("soap.endpoint.url"));
        
        System.out.println("Set endpoint URL to " + prop.getProperty("soap.endpoint.url"));

        TestServicePort client = (TestServicePort)jaxWsProxyFactoryBean.create();
        System.out.println("Invoking submit...");
        return client.submit(msgBindingObject);
    }
}
