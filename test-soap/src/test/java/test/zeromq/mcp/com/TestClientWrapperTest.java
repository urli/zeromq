/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.zeromq.mcp.com;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jmierwa
 */
public class TestClientWrapperTest {
    
    public TestClientWrapperTest() {
    }
    
    @Before
    public void setUp() {
    }

    /**
     * Test of submit method, of class TestClientWrapper.
     */
    @Test
    public void testSubmit() throws Exception {
        System.out.println("submit");
        String payloadString = "<TestMsg xmlns=\"urn:com.mcp.zeromq.test\"><TestMsg>Hello. This is a test.</TestMsg></TestMsg>";
        String expResult = "Response from mock service";
        String result = TestClientWrapper.submit(payloadString);
        assertEquals(expResult, result);
    }
    
}
