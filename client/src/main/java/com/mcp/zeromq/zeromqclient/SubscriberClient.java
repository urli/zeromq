/*
 * Copyright (c) 2020 by Mission Critical Partners and the Iowa Division of Criminal and Juvenile Justice Planning.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 * - Prior written consent by Mission Critical Partners is required.
 * - Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.mcp.zeromq.zeromqclient;

import com.mcp.zeromq.ZConstants;
import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Socket;

import java.util.StringTokenizer;

import static java.lang.String.format;
import static org.zeromq.ZMQ.SUB;
import static org.zeromq.ZMQ.context;

/**
 * Weather update client in Java
 * Connects SUB socket to tcp://localhost:5556
 * Collects weather updates and finds avg temp in zipcode
 *
 * @since 1.0
 */
public class SubscriberClient {

    public void send(String[] args) {
        Context context = context(1);
        System.out.println("Collecting updates from weather update server...");
        //  Socket to talk to server
        Socket subscriber = context.socket(SUB);
        subscriber.connect(ZConstants.URL.TCP_5556);
        //  Subscribe to zipCode, default is NYC, 10001
        String filter = (args != null && args.length > 0) ? args[0] : "";
        subscriber.subscribe(filter.getBytes());
        // Process 100 updates
        long totalTemp = 0;
        int updateNum = 0;
        while (updateNum < 100) {
            //  Use trim to remove the tailing '0' character
            String receivedStr = subscriber.recvStr(0).trim();
            StringTokenizer tokenizer = new StringTokenizer(receivedStr, " ");
            int zipCode = Integer.valueOf(tokenizer.nextToken());
            int temperature = Integer.valueOf(tokenizer.nextToken());
            int relHumidity = Integer.valueOf(tokenizer.nextToken());
            System.out.println(format("Weather update: zipCode: %d , temperature: %d , humidity: %d",
                    zipCode, temperature, relHumidity));
            totalTemp += temperature;
            updateNum++;
        }
        System.out.printf("Average Temperature for zipCode %s was %d", filter, (int) (totalTemp / updateNum));
        subscriber.close();
        context.term();
    }
}
