/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcp.zeromq.zeromqclient;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jmierwa
 */
public class ClientTest {
    
    public ClientTest() {
    }

    /**
     * Test of send method, of class Client.
     */
    @Test
    public void testSend() {
        System.out.println("send");
        String message = "Hello";
        Client instance = new Client();
        instance.send(message);
    }
    
}
