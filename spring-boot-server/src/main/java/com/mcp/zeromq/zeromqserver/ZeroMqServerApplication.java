package com.mcp.zeromq.zeromqserver;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZeroMqServerApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(ZeroMqServerApplication.class, args);
	}


	//access command line arguments
	@Override
	public void run(String... args) throws Exception {

		SimpleServer.main(args);

	}
}
