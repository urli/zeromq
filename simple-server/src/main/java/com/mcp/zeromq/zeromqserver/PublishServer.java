/*
 * Copyright (c) 2020 by Mission Critical Partners and the Iowa Division of Criminal and Juvenile Justice Planning.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 * - Prior written consent by Mission Critical Partners is required.
 * - Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.mcp.zeromq.zeromqserver;

import static com.mcp.zeromq.ZConstants.URL.IPC_WEATHER;
import static com.mcp.zeromq.ZConstants.URL.TCP_5556;
import static java.lang.String.format;
import static java.lang.Thread.currentThread;
import static java.lang.Thread.sleep;
import static org.zeromq.ZMQ.PUB;
import static org.zeromq.ZMQ.context;

import java.security.SecureRandom;

import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Socket;

/**
 * Weather update server in Java
 * Binds PUB socket to tcp://*:5556
 * Publishes random weather updates
 *
 * @since 1.0
 */
public class PublishServer {

    public static void main(String[] args) throws InterruptedException {
        //  Prepare our context and publisher
        Context context = context(1);
        Socket publisher = context.socket(PUB);
        System.out.println("Waking up and starting to send to subscribers");
        publisher.bind(TCP_5556);
        publisher.bind(IPC_WEATHER);
        SecureRandom random = new SecureRandom();
        while (!currentThread().isInterrupted()) {
            String update = prepareWeatherUpdate(random);
            publisher.send(update, 0);
            System.out.println("Sent " + update);
        }
        publisher.close();
        context.term();
    }

    private static String prepareWeatherUpdate(SecureRandom random) {
        //  Get values that will fool the boss
        int zipCode, temperature, relHumidity;
        zipCode = 10000 + random.nextInt(10000);
        temperature = random.nextInt(215) - 80 + 1;
        relHumidity = random.nextInt(50) + 10 + 1;
        //  Send message to all subscribers
        return format("%05d %d %d", zipCode, temperature, relHumidity);
    }
}
