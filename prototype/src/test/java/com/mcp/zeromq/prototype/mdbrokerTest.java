/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mcp.zeromq.prototype;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import org.zeromq.ZMsg;

import java.lang.reflect.Array;
import java.net.URL;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import com.google.common.io.Resources;
import java.nio.charset.StandardCharsets;

/**
 *
 * @author jmierwa
 */
public class mdbrokerTest {
    
    public mdbrokerTest() {
    }
    
    @BeforeEach
    public void setUp() {
    }

    /**
     * Test of main method, of class mdbroker.
     */
    @org.junit.jupiter.api.Test
    public void testMain() throws IOException, JSONException {
        Boolean run = true;
        JSONObject jsonObject;
        StringBuilder builder = new StringBuilder();

        Thread brokerThread = new Thread(() -> {
            String[] args = new String[]{"-v"};
            mdbroker.main(args);
        });
        brokerThread.start();
        
        Thread workerThread = new Thread(() -> {
            String[] args = new String[]{"-v"};
            try {
                mdworker.main(args);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        workerThread.start();

//        String[] args = new String[]{"-v", "<FailMsg xmlns=\"urn:com.mcp.zeromq.test.fail\"><TestMsg>Hello. This is an intentional failure.</FailMsg></FailMsg>"};
//        String[] args = new String[]{"-v", "<TestMsg xmlns=\"urn:com.mcp.zeromq.test\"><TestMsg>Hello. This is a test.</TestMsg></TestMsg>"};
//        String[] args = new String[]{"-v"};
        ArrayList<String> messages = new ArrayList<>();
        ArrayList<RetryObject> failures = new ArrayList<>();
        messages.add("-v");
        URL url = Resources.getResource("JSONmessages.txt");
        String data = Resources.toString(url, StandardCharsets.UTF_8);
        String[] lines = data.split("\n");
        for (String line: lines) {
            builder.append("<TestMsg xmlns=\"urn:com.mcp.zeromq.test\">");
            jsonObject = new JSONObject(line);
            builder.append(XML.toString(jsonObject));
            builder.append("</TestMsg>");
            messages.add(builder.toString());
            builder.setLength(0);
            failures = mdclient2.sendMessages(messages);
        }
//        mdclient2.main(args);
        while (run) {

            if (!failures.isEmpty()) {
                failures = mdclient2.handleRetries(failures);
            }

            if (failures.isEmpty()) {
                run = false;
            }
        }
        // TODO: Pass the error back after 3 retries!

        System.out.println("finished");
    }
    
}

// have test be able to input payload
// run client main after input
// have input section on while loop
// manual control on retries for now
// choose whether or not to build valid message
// recv still occurs right after send
// json to xml