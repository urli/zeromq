package com.mcp.zeromq.prototype;

import org.joda.time.LocalDateTime;
import org.zeromq.ZMsg;

import java.util.Map;

public class RetryObject {
    // use Jodatime not Date
    // persist the actual error that occurred, to send back to app in case retry countmax is reached
    private LocalDateTime retryDate;
    private ZMsg failedMsg;
    // max retry count?
    private int retryCount = 0;

    public RetryObject(ZMsg failedMsg) {
        this.failedMsg = failedMsg;
        LocalDateTime date = LocalDateTime.now().plusSeconds(2);
        this.retryDate = date;
    }

    public ZMsg getFailedMsg() {
        return this.failedMsg;
    }

    public LocalDateTime getRetryDate() {
        return this.retryDate;
    }

    public int getRetryCount() {
        return this.retryCount;
    }

    public void increment() {
        this.retryCount++;
        LocalDateTime date = LocalDateTime.now().plusSeconds(2);
        this.retryDate = date;
    }
}
