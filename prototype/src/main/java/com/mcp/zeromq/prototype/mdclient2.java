package com.mcp.zeromq.prototype;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.zeromq.ZMsg;

import java.util.ArrayList;

/**
 * Majordomo Protocol client example, asynchronous. Uses the mdcli API to hide
 * all MDP aspects
 */

public class mdclient2
{
    // invoke this from another class, which will
    // how to get async behavior to us so the client gets the reply, and process it after doing something else, like calling "send"
    //
    public static ArrayList<RetryObject> retries = new ArrayList<>();

    public static void main(String[] args)
    {
//        boolean run = true;
//        String message;
//        String errMessage;
//        boolean verbose = (args.length > 0 && "-v".equals(args[0]));
//        mdcliapi2 clientSession = new mdcliapi2("tcp://localhost:5555", verbose);
//
//        if (args.length > 1) {
//            message = args[1];
//            ZMsg request = new ZMsg();
//            request.addString(message);
//            System.out.println("Client sending original message");
//            clientSession.send("echo", request);
//
////            for (int count = 0; count < 10; count++) {
//                System.out.println("Client RECV");
//                ZMsg reply = clientSession.recv();
//                if (reply != null) {
//                    if (StringUtils.equals(reply.getLast().toString(), "Error")) {
//                        reply.removeLast(); // gets rid of error designation
//                        reply.removeLast(); // gets rid of exception
////                        System.out.println(reply.getLast().toString() + "ASDF");
//                        retries.add(new RetryObject(reply));
//                        run = true;
////                        System.out.println("POP!: " + reply.popString());
//
////                        ~retries.add(new RetryObject(reply));
////                        // go through list of retries and retry them
////                        // check retry count, time, then send
//                    }
//                    // process failure, identify it as a failure
////                    reply.destroy();
//                }
////                else break; // Interrupt or failure
//
////            }
//        } else {
//           System.out.println("No message was provided as a parameter to send");
//        }
//
//        // TODO: not just using a while loop to wait on retries to be valid
//        // TODO: Storage of failed retries?
//        while (run) {
//            if (!retries.isEmpty()) {
////                System.out.println("Retries not empty");
//                for (RetryObject retry : retries) {
//                    // retry when needed
//                    if (retry.getRetryDate().isBefore(LocalDateTime.now())) {
//                        ZMsg request = new ZMsg();
////                        System.out.println(retry.getFailedMsg() + "asdf");
//                        message = retry.getFailedMsg().popString();
//                        retry.getFailedMsg().addLast(message);
////                        System.out.println(message + "ASDF");
//                        request.addString(message);
//                        System.out.println("Client Sending retry message");
//                        clientSession.send("echo", request);
//
//                        System.out.println("Client RECV");
//                        ZMsg reply = clientSession.recv();
//                        if (reply != null) {
//                            // if the retry fails, call increment()
//                            if (StringUtils.equals(reply.getLast().toString(), "Error")) {
//                                retry.increment();
//                            }
//                            // else it was a success
//                            else {
//                                run = false;
//                            }
//                        }
//
//                        // TODO: This needs to be way more robust
//                        if (retry.getRetryCount() >= 3) {
//                            System.out.println("Reached max retries (3)");
//                            run = false;
//                            reply.removeLast(); // remove error designation for processing
//                            errMessage = reply.getLast().toString();
//                            reply.destroy();
//                        }
////                        else {
////                            reply.removeLast(); // remove error designation for processing
////                            System.out.println("YOOOOOOOOOOO:" + reply.getLast().toString());
////                        }
//                    }
//                }
//                // TODO: Mark which ones to clean up when there's more than one failure
//                if (!run) {
//                    retries.remove(0);
//                }
//            }
//            else {
//                run = false;
//            }
//        }
//
////        System.out.printf("%d request sent/replies checked for \n");
//        clientSession.destroy();
//        args = new String[] {"-v"};
    }

    // TODO: Handle retries again
    public static ArrayList<RetryObject> sendMessages (ArrayList<String> payloads) {
//        boolean verbose = (payloads.size() > 0 && "-v".equals(payloads.get(0)));
        boolean verbose = true;
        payloads.remove(0);
        mdcliapi2 clientSession = new mdcliapi2("tcp://localhost:5555", verbose);

        if (payloads.size() > 0) {
            for (String payload: payloads) {
                ZMsg request = new ZMsg();
                request.addString(payload);
                System.out.println("Client sending original message");
                clientSession.send("echo", request);
                request.destroy();

//            for (int count = 0; count < 10; count++) {
                System.out.println("Client RECV");
                ZMsg reply = clientSession.recv();
                if (reply != null) {
                    if (StringUtils.equals(reply.getLast().toString(), "Error")) {
                        reply.removeLast(); // gets rid of error designation
                        reply.removeLast(); // gets rid of exception
                        retries.add(new RetryObject(reply));
                    }
                    else {
                        reply.destroy();
                    }
                    // process failure, identify it as a failure
//                    reply.destroy();
                }
            }
        } else {
            System.out.println("No message was provided as a parameter to send");
        }

        clientSession.destroy();
        return retries;
    }

    public static ArrayList<RetryObject> handleRetries(ArrayList<RetryObject> retries) {
        boolean verbose = true;
        String message;
        mdcliapi2 clientSession = new mdcliapi2("tcp://localhost:5555", verbose);

        for (RetryObject retry : retries) {
            if (retry.getRetryDate().isBefore(LocalDateTime.now())) {
                ZMsg request = new ZMsg();
                message = retry.getFailedMsg().popString();
                retry.getFailedMsg().addLast(message);
                request.addString(message);
                clientSession.send("echo", request);

                System.out.println("Client RECV");
                ZMsg reply = clientSession.recv();
                if (reply != null) {
                    if (StringUtils.equals(reply.getLast().toString(), "Error")) {
                        retry.increment();
                        System.out.println("Retry count: " + retry.getRetryCount());
                    }
                    else {
                        reply.destroy();
                    }
                }
            }

            if (retry.getRetryCount() >= 3) {
                System.out.println("Reached max retries (3)");
                retries.clear();
            }
        }

        clientSession.destroy();
        return retries;
    }

}
