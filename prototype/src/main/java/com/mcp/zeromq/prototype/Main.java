package com.mcp.zeromq.prototype;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.*;
import com.google.common.io.Resources;

public class Main {
    public static void main(String[] args) throws IOException {
        Boolean run = true;
        String payload;
        JSONObject jsonObject;
        StringBuilder builder = new StringBuilder();

        String message;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter your message: ");
        message = reader.readLine();

        Thread brokerThread = new Thread(() -> {
            String[] arr = new String[]{"-v"};
            mdbroker.main(arr);
        });
        brokerThread.start();

        Thread workerThread = new Thread(() -> {
            String[] arr = new String[]{"-v"};
            try {
                mdworker.main(arr);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        workerThread.start();

        ArrayList<String> messages = new ArrayList<>();
        ArrayList<RetryObject> failures;
        messages.add("-v");

        builder.append("<TestMsg xmlns=\"urn:com.mcp.zeromq.test\">");
        jsonObject = new JSONObject(message);
        builder.append(XML.toString(jsonObject));
        builder.append("</TestMsg>");
        messages.add(builder.toString());

        failures = mdclient2.sendMessages(messages);
//        }
//        mdclient2.main(args);
        while (run) {

//            if (!failures.isEmpty()) {
//                failures = mdclient2.handleRetries(failures);
//            }
//
//            if (failures.isEmpty()) {
//                run = false;
//            }
            run = false;
        }
        // TODO: Pass the error back after 3 retries!

        System.out.println("finished");
    }
}
