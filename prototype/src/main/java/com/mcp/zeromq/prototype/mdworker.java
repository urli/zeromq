package com.mcp.zeromq.prototype;

import java.nio.charset.Charset;
import org.zeromq.ZMsg;
import test.zeromq.mcp.com.TestClientWrapper;

/**
* Majordomo Protocol worker example. Uses the mdwrk API to hide all MDP aspects
*
*/
public class mdworker
{

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception
    {
//        boolean verbose = (args.length > 0 && "-v".equals(args[0]));
        boolean verbose = false;
        mdwrkapi workerSession = new mdwrkapi("tcp://localhost:5555", "echo", verbose);

        ZMsg reply = null;
        while (!Thread.currentThread().isInterrupted()) {
            ZMsg request = workerSession.receive(reply);
            if (request == null)
                break; //Interrupted

            // TODO: Figure out how to build ZMsg.reply.
            // TODO: Add something to the reply to signify a failure
            reply = request; //  Echo is complex :-)

            try {
                // TODO: Figure out how to get the WS payload from the message and how failures get handled.
                String response = TestClientWrapper.submit(request.getLast().getString(Charset.defaultCharset()));
                reply.addLast("Success");
            } catch (Exception e) {
                reply.addLast(e.toString());
                reply.addLast("Error");
            }
        }
        workerSession.destroy();
    }
}
